#!/bin/bash

if [ "$1" ]
then
    if [ -e "./config/$1/" ]
    then
        cp -rf "./config/$1/fastlane" "./android"
        cp -rf "./config/$1/google-services.json" "./android"
        echo "Config successfully changed to $1"
    else
        echo "Config file not found"
    fi
else
    echo "You should pass staging name(e.g. test, dev, etc)"
fi