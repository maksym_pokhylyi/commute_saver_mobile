import produce from 'immer';

import { INITIAL_TIMER } from '../actions/timer';
import { START_TIMER, STOP_TIMER, UPDATE_TIMER } from '../constants/timer';

const initialState = {
    timer          : INITIAL_TIMER,
    isTimerRunning : false
};

export default produce((draft, action) => {
    const { type, payload } = action;

    switch (type) {
        case UPDATE_TIMER:
            draft.timer = payload.timer;
            break;
        case START_TIMER:
            draft.isTimerRunning = true;
            break;
        case STOP_TIMER:
            draft.isTimerRunning = false;
            draft.timer = INITIAL_TIMER;
            break;

        default:
            break;
    }
}, initialState);
