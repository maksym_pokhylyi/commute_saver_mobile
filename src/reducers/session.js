import produce from 'immer';

import { CHECK_SESSION_SUCCESS } from '../constants/session';

const initialState = {
    isSessionExist : false
};

export default produce((draft, action) => {
    const { type } = action;

    switch (type) {
        case CHECK_SESSION_SUCCESS:
            draft.isSessionExist = true;
            break;
        default:
            break;
    }
}, initialState);
