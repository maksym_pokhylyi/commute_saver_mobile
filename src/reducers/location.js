import produce from 'immer';

import { UPDATE_CURRENT_LOCATION, UPDATE_LOCATION_PERMISSION } from '../constants/location';

const initialState = {
    currentLocation          : null,
    locationPermissionStatus : 'denied'
};

export default produce((draft, action) => {
    const { type, payload } = action;

    switch (type) {
        case UPDATE_CURRENT_LOCATION:
            draft.currentLocation = payload.location;
            break;
        case UPDATE_LOCATION_PERMISSION:
            draft.locationPermissionStatus = payload.status;
            break;
        default:
            break;
    }
}, initialState);
