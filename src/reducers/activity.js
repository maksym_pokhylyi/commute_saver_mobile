import produce from 'immer';

import {
    REPLACE_ACTIVITY_LOG,
    UPDATE_ACTIVITY_LOG,
    UPDATE_ACTIVITY_PERMISSION
} from '../constants/activity';

const initialState = {
    activityLog              : [],
    activityPermissionStatus : 'denied'
};

export default produce((draft, action) => {
    const { type, payload } = action;

    switch (type) {
        case UPDATE_ACTIVITY_LOG:
            draft.activityLog.push(payload.activity);
            break;
        case REPLACE_ACTIVITY_LOG:
            draft.activityLog = payload.activity;
            break;
        case UPDATE_ACTIVITY_PERMISSION:
            draft.activityPermissionStatus = payload.status;
            break;
        default:
            break;
    }
}, initialState);
