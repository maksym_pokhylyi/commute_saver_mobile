import { combineReducers } from 'redux';

import activity from './activity';
import location from './location';
import session from './session';
import snackBar from './snackBar';
import timer from './timer';

export default combineReducers({
    activity,
    location,
    session,
    snackBar,
    timer
});
