/* eslint-disable no-magic-numbers */
import BackgroundGeolocation from '@darron1217/react-native-background-geolocation';
import moment from 'moment';
import BackgroundService from 'react-native-background-actions';
import { check, PERMISSIONS, request, RESULTS } from 'react-native-permissions';

import {
    replaceActivityLog,
    updateActivityLog,
    updateActivityPermission
} from '../actions/activity';
import { updateCurrentLocation, updateLocationPermission } from '../actions/location';
import { dismissMessage, showMessage } from '../actions/snackBar';
import { INITIAL_TIMER, startTimer, stopTimer, updateTimer } from '../actions/timer';
import colors from '../assets/constants/colors';
import store from '../store';
import Storage from './AsyncStorage';

class TrackingManager {
    constructor() {
        this.interval = null;
        this.bgTimeout = null;
    }

    static async initialize() {
        await this.initializePermission();
        await this.initializeLocation();
        await this.initializeActivity();
        await this.initializeTimer();
    }

    static async initializePermission() {
        const { dispatch } = store;
        const ACTIVITY_PERMISSION = await this.getActivityPermissionConstant();

        let locationGranted = await this.checkPermission(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);

        let activityGranted = await this.checkPermission(ACTIVITY_PERMISSION);

        const isPermissionsGranted = locationGranted === RESULTS.GRANTED
            && activityGranted === RESULTS.GRANTED;

        if (!isPermissionsGranted) {
            locationGranted = await this.requestPermission(
                PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);

            activityGranted = await this.requestPermission(
                ACTIVITY_PERMISSION);
        }

        await dispatch(updateLocationPermission(locationGranted));
        await dispatch(updateActivityPermission(activityGranted));

        return { locationGranted, activityGranted };
    }

    static async initializeLocation() {
        const { dispatch } = store;
        const location = await this.getItemFromStorage('location');

        if (location) {
            const { latitude, longitude, speed } = location;

            await dispatch(updateCurrentLocation({ latitude, longitude, speed }));
        }
    }

    static async initializeActivity() {
        const { dispatch } = store;
        const activity = await this.getItemFromStorage('activity');

        if (activity) {
            await dispatch(replaceActivityLog(activity));
        }
    }

    static async initializeTimer() {
        const { dispatch } = store;
        const timer = await this.getItemFromStorage('timer');

        if (timer) {
            const currentTimer = await this.getTimerAfterBackground();

            clearInterval(this.interval);
            await dispatch(updateTimer(currentTimer));
            this.start();
        }
    }

    static async checkPermissions() {
        const { dispatch } = store;
        const ACTIVITY_PERMISSION = await this.getActivityPermissionConstant();

        const locationGranted = await this.checkPermission(
            PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);

        const activityGranted = await this.checkPermission(
            ACTIVITY_PERMISSION);

        await dispatch(updateLocationPermission(locationGranted));
        await dispatch(updateActivityPermission(activityGranted));
    }

    static async getActivityPermissionConstant() {
        const isActivityAvailable = await this.checkPermission(
            PERMISSIONS.ANDROID.ACTIVITY_RECOGNITION) !== RESULTS.UNAVAILABLE;

        const ACTIVITY_PERMISSION = isActivityAvailable
            ? PERMISSIONS.ANDROID.ACTIVITY_RECOGNITION
            : PERMISSIONS.ANDROID.BODY_SENSORS;

        return ACTIVITY_PERMISSION;
    }

    static async start() {
        const { dispatch } = store;

        await dispatch(startTimer());

        this.interval = setInterval(this.updateTimer, 1000);

        this.updateTimer();

        await this.removeTrackingListeners();
        await this.configureLocationTracking();
        await this.addTrackingListeners();
        BackgroundGeolocation.start();
    }

    static async stop(isNeedToCleanStorage = true) {
        const { dispatch } = store;

        if (isNeedToCleanStorage) {
            await this.removeItemFromStorage('timer');
            await this.removeItemFromStorage('activity');
            await this.removeItemFromStorage('location');
        }

        clearInterval(this.interval);

        await dispatch(stopTimer());
        await dispatch(dismissMessage());
        await dispatch(replaceActivityLog([]));
        await dispatch(updateCurrentLocation(null));
        await this.removeTrackingListeners();
        await BackgroundService.stop();
        BackgroundGeolocation.stop();
    }

    static updateTimer = () => {
        const { dispatch } = store;
        const { timer } = store.getState().timer;
        const minutes = this.getCorrectTime('minutes', timer);
        const seconds = this.getCorrectTime('seconds', timer);
        const message = `${minutes}:${seconds}`;

        dispatch(showMessage({ message }));

        if (timer === INITIAL_TIMER) {
            this.updateLocation();
        }

        if (timer === 0) {
            dispatch(updateTimer(INITIAL_TIMER));
        } else {
            dispatch(updateTimer(timer - 1));
        }
    }

    static async updateLocation() {
        await this.getLocation(async ({ latitude, longitude, speed }) => {
            const { dispatch } = store;

            await dispatch(updateCurrentLocation({ latitude, longitude, speed }));
            this.setItemToStorage('location', { latitude, longitude, speed });
        });
    }

    static getCorrectTime = (target, value) => {
        let output;

        switch (target) {
            case 'minutes':
                output = parseInt(value / 60, 10);

                return output < 10 ? `0${output}` : output;

            default:
                output = parseInt(value % 60, 10);

                return output < 10 ? `0${output}` : output;
        }
    }

    static async getTimerAfterBackground() {
        const { time, timer } = await this.getItemFromStorage('timer');
        const then = moment(time);
        const now = moment();
        const difference = now.diff(then, 'seconds');

        if (difference < timer) {
            return timer - difference;
        }

        return difference % INITIAL_TIMER;
    }

    static async getLocation(callback) {
        BackgroundGeolocation.getCurrentLocation(callback);
    }

    static async requestPermission(permission) {
        const granted = await request(permission);

        return granted;
    }

    static async checkPermission(permission) {
        const granted = await check(permission);

        return granted;
    }

    static async configureLocationTracking() {
        BackgroundGeolocation.configure({
            desiredAccuracy       : BackgroundGeolocation.HIGH_ACCURACY,
            stationaryRadius      : 1,
            distanceFilter        : 1,
            notificationTitle     : 'Location tracking',
            notificationText      : 'Background location tracking enabled',
            notificationIconColor : colors.PRIMARY,
            stopOnTerminate       : false,
            locationProvider      : 1,
            interval              : 10000,
            fastestInterval       : 5000,
            activitiesInterval    : 1000
        });
    }

    static async showActivityNotification(taskDesc) {
        await BackgroundService.updateNotification({ taskDesc });
    }

    static async addTrackingListeners() {
        const { dispatch } = store;
        const options = {
            taskName  : 'Activity tracking',
            taskTitle : 'Activity tracking',
            taskDesc  : 'Background activity tracking enabled',
            color     : colors.PRIMARY,
            taskIcon  : {
                name : 'ic_launcher',
                type : 'mipmap'
            }
        };

        // background and foreground listeners is a lifehack
        // to track activity when the app is killed
        BackgroundGeolocation.on('background', () => {
            this.bgTimeout = setTimeout(async () => {
                await this.stop(false);
                await this.start();
            }, 5000);
        });

        BackgroundGeolocation.on('foreground', async () => {
            clearTimeout(this.bgTimeout);
            await this.stop(false);
            await this.start();
        });

        await BackgroundService.start(async () => {
            await new Promise(() => {
                BackgroundGeolocation.on('activity', async activity => {
                    await dispatch(updateActivityLog(activity));
                });
            });
        }, options);
    }

    static async removeTrackingListeners() {
        BackgroundGeolocation.removeAllListeners();
    }

    static async getItemFromStorage(key) {
        const value = await Storage.getItem(key);

        return value;
    }

    static async setItemToStorage(key, value) {
        await Storage.setItem(key, value);
    }

    static async removeItemFromStorage(key) {
        await Storage.removeItem(key);
    }

    static setTimerToStorage = async timer => {
        await Storage.setItem('timer', { timer, time: moment() });
    }
}

export default TrackingManager;
