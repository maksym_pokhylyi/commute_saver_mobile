import {
    START_TIMER,
    STOP_TIMER,
    UPDATE_TIMER
} from '../constants/timer';

export const INITIAL_TIMER = 1799;

export function updateTimer(timer) {
    return {
        type    : UPDATE_TIMER,
        payload : { timer }
    };
}

export function startTimer() {
    return {
        type : START_TIMER
    };
}

export function stopTimer() {
    return {
        type : STOP_TIMER
    };
}
