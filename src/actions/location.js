import { UPDATE_CURRENT_LOCATION, UPDATE_LOCATION_PERMISSION } from '../constants/location';

export function updateCurrentLocation(location) {
    return {
        type    : UPDATE_CURRENT_LOCATION,
        payload : { location }
    };
}

export function updateLocationPermission(status) {
    return async dispatch => {
        dispatch({
            type    : UPDATE_LOCATION_PERMISSION,
            payload : { status }
        });
    };
}
