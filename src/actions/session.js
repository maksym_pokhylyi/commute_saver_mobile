
import { CHECK_SESSION_SUCCESS } from '../constants/session';
import TrackingManager from '../utils/TrackingManager';

export function checkSession() {
    return async dispatch => {
        await TrackingManager.initialize();
        dispatch({ type: CHECK_SESSION_SUCCESS });
    };
}
