import {
    REPLACE_ACTIVITY_LOG,
    UPDATE_ACTIVITY_LOG,
    UPDATE_ACTIVITY_PERMISSION
} from '../constants/activity';
import Storage from '../utils/AsyncStorage';

export function updateActivityLog(activity) {
    return async (dispatch, getState) => {
        const { activityLog } = await getState().activity;
        const nextActivityLog = [ ...activityLog, activity ];
        const prevActivity = activityLog[activityLog.length - 1];
        const isActivityChanged = !prevActivity
            || prevActivity.type !== activity.type
            || prevActivity.confidence !== activity.confidence;

        if (isActivityChanged) {
            Storage.setItem('activity', nextActivityLog);

            return dispatch({
                type    : UPDATE_ACTIVITY_LOG,
                payload : { activity }
            });
        }
    };
}

export function replaceActivityLog(activity) {
    return {
        type    : REPLACE_ACTIVITY_LOG,
        payload : { activity }
    };
}

export function updateActivityPermission(status) {
    return async dispatch => {
        dispatch({
            type    : UPDATE_ACTIVITY_PERMISSION,
            payload : { status }
        });
    };
}
