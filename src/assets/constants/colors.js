const PRIMARY = '#01C686';
const SECONDARY = '#C2DAC2';
const GREY = '#EAEDEE';
const TEXT_PRIMARY = '#134D68';
const TEXT_SECONDARY = '#374955';
const BACKGROUND = '#FFFFFF';
const LINE = '#CECFD1';

export default {
    PRIMARY,
    SECONDARY,
    TEXT_PRIMARY,
    TEXT_SECONDARY,
    GREY,
    BACKGROUND,
    LINE
};
