const MAIN = 'MAIN';
const TAB_NAVIGATION = 'TAB_NAVIGATION';

export default {
    MAIN,
    TAB_NAVIGATION
};
