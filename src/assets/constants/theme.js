import colors from './colors';

export default {
    Button : {
        containerStyle : {
            width        : '100%',
            height       : 70,
            borderRadius : 37
        },
        buttonStyle : {
            borderRadius : 37,
            width        : '100%',
            height       : 70,
            padding      : 18
        },
        titleStyle : {
            fontSize : 25,
            width    : '100%'
        }
    },
    colors : {
        primary : colors.PRIMARY
    }
};
