import { NavigationContainer } from '@react-navigation/native';
import React, { PureComponent } from 'react';
import { ThemeProvider } from 'react-native-elements';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider as StoreProvider } from 'react-redux';

import theme from './assets/constants/theme';
import AuthNavigation from './components/navigation/AuthNavigation';
import store from './store';

class App extends PureComponent {
    componentDidMount() {
    }

    render() {
        return (
            <SafeAreaProvider>
                <NavigationContainer>
                    <StoreProvider store={store}>
                        <ThemeProvider theme={theme}>
                            <AuthNavigation />
                        </ThemeProvider>
                    </StoreProvider>
                </NavigationContainer>
            </SafeAreaProvider>
        );
    }
}

export default App;
