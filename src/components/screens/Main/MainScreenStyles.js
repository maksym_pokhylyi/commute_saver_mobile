import { StyleSheet } from 'react-native';

import colors from '../../../assets/constants/colors';

export default StyleSheet.create({
    container : {
        flex              : 1,
        backgroundColor   : colors.BACKGROUND,
        justifyContent    : 'space-between',
        paddingHorizontal : 20,
        paddingBottom     : 20,
        paddingTop        : 55
    },
    content : {
        flex       : 1,
        width      : '100%',
        alignItems : 'center'
    },
    list : {
        width : '100%'
    },
    text : {
        fontSize     : 20,
        color        : colors.TEXT_PRIMARY,
        width        : '100%',
        marginBottom : 5
    },
    settingsButton : {
        marginTop         : 20,
        alignItems        : 'center',
        paddingVertical   : 10,
        paddingHorizontal : 40
    },
    settingsButtonText : {
        color      : colors.PRIMARY,
        fontWeight : 'bold',
        fontSize   : 20
    }
});
