/* eslint-disable max-len */
/* eslint-disable react/no-array-index-key */
/* eslint-disable no-nested-ternary */
import BackgroundGeolocation from '@darron1217/react-native-background-geolocation';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { FlatList, TouchableOpacity, View } from 'react-native';
import { RESULTS } from 'react-native-permissions';
import { connect } from 'react-redux';

import * as locationActions from '../../../actions/location';
import TrackingManager from '../../../utils/TrackingManager';
import Button from '../../ui-kit/Button';
import Text from '../../ui-kit/Text';
import styles from './MainScreenStyles';

class MainScreen extends PureComponent {
    static propTypes = {
        isTimerRunning           : PropTypes.bool.isRequired,
        locationPermissionStatus : PropTypes.string.isRequired,
        activityPermissionStatus : PropTypes.string.isRequired,
        currentLocation          : PropTypes.object,
        activityLog              : PropTypes.array
    };

    static defaultProps={
        currentLocation : {},
        activityLog     : []
    }

    constructor(props) {
        super(props);

        this.state = {
            isLoading : false
        };
    }

    handleShowAppSettings = () => BackgroundGeolocation.showAppSettings();

    handleButtonPress = async () => {
        const { isTimerRunning } = this.props;
        const isPermissionsGranted = this.getIsPermissionsGranted();

        this.updateIsLoading(true, async () => {
            try {
                if (!isPermissionsGranted) {
                    await TrackingManager.initializePermission();

                    const newPermissionsStatus = this.getIsPermissionsGranted();

                    if (!newPermissionsStatus) {
                        return;
                    }
                }

                if (!isTimerRunning) {
                    await TrackingManager.start();
                } else {
                    await TrackingManager.stop();
                }
            } finally {
                this.updateIsLoading(false);
            }
        });
    }

    getIsPermissionsGranted = () => {
        const { locationPermissionStatus, activityPermissionStatus } = this.props;

        return locationPermissionStatus === RESULTS.GRANTED
            && activityPermissionStatus === RESULTS.GRANTED;
    }

    getIsPermissionsNeverAsk = () => {
        const { locationPermissionStatus, activityPermissionStatus } = this.props;

        return locationPermissionStatus === RESULTS.BLOCKED
            || activityPermissionStatus === RESULTS.BLOCKED;
    }

    getButtonTitle = () => {
        const { isTimerRunning } = this.props;
        const isPermissionsGranted = this.getIsPermissionsGranted();
        const title = !isPermissionsGranted
            ? 'Allow permissions'
            : isTimerRunning
                ? 'Done'
                : 'Start';

        return title;
    }

    getContentText = () => {
        const isPermissionsGranted = this.getIsPermissionsGranted();
        const isPermissionsNeverAsk = this.getIsPermissionsNeverAsk();

        if (!isPermissionsGranted) {
            return 'Need permission to access your location and physical activity';
        }

        if (isPermissionsNeverAsk) {
            // eslint-disable-next-line max-len
            return 'Please give access to location and physical activity in the application settings';
        }
    }

    getKeyExtractor = (item, index) => index;

    updateIsLoading = (isLoading, callback) => {
        this.setState({ isLoading }, callback);
    }

    renderEmptyContent = () => {
        const text = this.getContentText();

        return (
            <Text style={styles.text}>{text}</Text>
        );
    }

    renderLogItem = ({ item, index }) => {
        const { type, confidence } = item;

        return type !== 'UNKNOWN' && (
            <Text style={styles.text} key={index}>
                {`${type} ${type && ','} ${confidence || ''}`}
            </Text>
        );
    }

    renderListHeader = () => {
        return (
            <Text style={styles.text}>
                Activity log:
            </Text>
        );
    }

    renderContent = () => {
        const { activityLog } = this.props;

        return (

            <FlatList
                data={activityLog}
                renderItem={this.renderLogItem}
                keyExtractor={this.getKeyExtractor}
                style={styles.list}
                ListHeaderComponent={this.renderListHeader}
            />
        );
    }

    renderSettingsButton = () => {
        return (
            <TouchableOpacity
                onPress={this.handleShowAppSettings}
                style={styles.settingsButton}
            >
                <Text style={styles.settingsButtonText}>Go to settings</Text>
            </TouchableOpacity>
        );
    }

    render() {
        const { currentLocation } = this.props;
        const { isLoading } = this.state;
        const isShowSettingsButton = this.getIsPermissionsNeverAsk();
        const isButtonDisabled = isLoading || this.getIsPermissionsNeverAsk();

        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    {currentLocation
                        ? this.renderContent()
                        : this.renderEmptyContent()
                    }

                    {isShowSettingsButton && this.renderSettingsButton()}
                </View>

                <Button
                    title={this.getButtonTitle()}
                    onPress={this.handleButtonPress}
                    loading={isLoading}
                    disabled={isButtonDisabled}
                />
            </View>
        );
    }
}

export default connect(
    state => ({
        isTimerRunning           : state.timer.isTimerRunning,
        locationPermissionStatus : state.location.locationPermissionStatus,
        activityPermissionStatus : state.activity.activityPermissionStatus,
        currentLocation          : state.location.currentLocation,
        activityLog              : state.activity.activityLog
    }),
    { ...locationActions }
)(MainScreen);
