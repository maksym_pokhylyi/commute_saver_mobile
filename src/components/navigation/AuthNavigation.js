import { createStackNavigator } from '@react-navigation/stack';
import React, { PureComponent } from 'react';

import colors from '../../assets/constants/colors';
import screens from '../../assets/constants/screens';
import MainLayout from '../layouts/MainLayout';
import TabNavigation from './TabNavigation';

const Stack = createStackNavigator();

const stackOptions = {
    headerTitleStyle : {
        color : colors.TEXT_PRIMARY
    },
    headerStyle : {
        backgroundColor   : colors.PRIMARY,
        borderBottomColor : colors.LINE
    }
};

const withoutHeaderOptions = {
    headerShown : false
};

class AuthNavigation extends PureComponent {
    render() {
        return (
            <MainLayout>
                {({ initialRoute }) => (
                    <Stack.Navigator screenOptions={stackOptions}>
                        <Stack.Screen
                            name={screens.TAB_NAVIGATION}
                            options={withoutHeaderOptions}
                        >
                            {() => <TabNavigation initialRoute={initialRoute} />}
                        </Stack.Screen>
                    </Stack.Navigator>
                )}
            </MainLayout>
        );
    }
}

export default AuthNavigation;
