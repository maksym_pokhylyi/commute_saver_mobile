import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

import colors from '../../assets/constants/colors';
import screens from '../../assets/constants/screens';
import MainScreen from '../screens/Main';
import Icon from '../ui-kit/Icon/Icon';

const Tab = createBottomTabNavigator();

const screenOptions = {
    tabBarActiveTintColor   : colors.TEXT_PRIMARY,
    tabBarInactiveTintColor : colors.TEXT_SECONDARY,
    allowFontScalling       : false,
    tabBarStyle             : {
        minHeight       : 65,
        backgroundColor : colors.BACKGROUND,
        borderTopColor  : colors.LINE,
        borderTopWidth  : 1
    },
    tabBarItemStyle : {
        paddingVertical : 10,
        alignItems      : 'center',
        justifyContent  : 'center'
    },
    tabBarIconStyle : {
        width : '100%'
    },
    tabBarLabelStyle : {
        fontWeight : 'bold'
    },
    headerTitleStyle : {
        color : colors.TEXT_PRIMARY
    },
    headerStyle : {
        backgroundColor   : colors.PRIMARY,
        borderBottomColor : colors.LINE,
        height            : 60
    }
};

class TabNavigation extends PureComponent {
    static propTypes = {
        initialRoute : PropTypes.string.isRequired
    };

    render() {
        const { initialRoute } = this.props;

        return (
            <Tab.Navigator
                screenOptions={screenOptions}
                backBehavior='none'
                initialRouteName={initialRoute}
            >
                <Tab.Screen
                    name={screens.MAIN}
                    component={MainScreen}
                    options={{
                        tabBarLabel : 'Main',
                        tabBarIcon  : ({ color, size }) => (
                            <Icon
                                color={color}
                                size={size}
                                name='home'
                                type='ionicon'
                            />
                        )
                    }}
                />
            </Tab.Navigator>
        );
    }
}

export default TabNavigation;
