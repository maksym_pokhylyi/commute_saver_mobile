import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import { AppState, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { connect } from 'react-redux';

import * as sessionActions from '../../../actions/session';
import screens from '../../../assets/constants/screens';
import TrackingManager from '../../../utils/TrackingManager';
import LoadingIndicator from '../../ui-kit/LoadingIndicator';
import SnackBar from '../../ui-kit/SnackBar';
import styles from './MainLayoutStyles';

class MainLayout extends PureComponent {
    static propTypes = {
        children       : PropTypes.func.isRequired,
        timer          : PropTypes.number.isRequired,
        isTimerRunning : PropTypes.bool.isRequired,
        isSessionExist : PropTypes.bool.isRequired,
        checkSession   : PropTypes.func.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            initialRoute : screens.MAIN
        };
    }

    componentDidMount() {
        const { checkSession } = this.props;

        checkSession();

        AppState.addEventListener('change', this.handleAppStateChage);
    }

    componentWillUnmount() {
        AppState.removeEventListener('change', this.handleAppStateChage);
    }

    handleAppStateChage = nextAppState => {
        const { timer, isTimerRunning } = this.props;
        const isNeedToSaveData = nextAppState === 'background'
            && isTimerRunning;

        if (isNeedToSaveData) {
            TrackingManager.setTimerToStorage(timer);
        } else {
            TrackingManager.checkPermissions();
        }
    }

    render() {
        const { children, isSessionExist } = this.props;
        const { initialRoute } = this.state;

        if (!isSessionExist) {
            return <LoadingIndicator />;
        }

        return (
            <SafeAreaView style={styles.container}>
                <SnackBar />

                <View style={styles.container}>
                    {children({ initialRoute })}
                </View>
            </SafeAreaView>
        );
    }
}

export default connect(
    state => ({
        timer          : state.timer.timer,
        isTimerRunning : state.timer.isTimerRunning,
        isSessionExist : state.session.isSessionExist
    }),
    { ...sessionActions }
)(MainLayout);
