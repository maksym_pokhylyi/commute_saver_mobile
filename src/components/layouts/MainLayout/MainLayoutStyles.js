import { StyleSheet } from 'react-native';

import colors from '../../../assets/constants/colors';

export default StyleSheet.create({
    container : {
        flex            : 1,
        backgroundColor : colors.BACKGROUND
    }
});
