import { StyleSheet } from 'react-native';

import colors from '../../../assets/constants/colors';

export default StyleSheet.create({
    container : {
        minWidth        : '100%',
        height          : 35,
        backgroundColor : colors.GREY,
        position        : 'absolute',
        left            : 0,
        zIndex          : 1,
        justifyContent  : 'center',
        alignItems      : 'center'
    },
    message : {
        fontSize  : 18,
        color     : colors.TEXT_SECONDARY,
        width     : '100%',
        textAlign : 'center'
    }
});
