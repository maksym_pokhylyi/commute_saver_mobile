import React, { PureComponent } from 'react';
import { Button } from 'react-native-elements';

class CustomButton extends PureComponent {
    render() {
        return (
            <Button
                {...this.props}
                loadingProps={{ size: 'large' }}
            />
        );
    }
}

export default CustomButton;
