import React, { PureComponent } from 'react';
import { ActivityIndicator, View } from 'react-native';

import colors from '../../../assets/constants/colors';
import styles from './LoadingIndicatorStyles';

class LoadingIndicator extends PureComponent {
    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator
                    color={colors.PRIMARY}
                    size='large'
                />
            </View>
        );
    }
}

export default LoadingIndicator;
